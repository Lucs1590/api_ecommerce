const express = require("express");
const morgan = require("morgan");
const app = express();
var models = require('express-cassandra');

const usuarioRoute = require('./routes/usuarios-route');
const produtoRoute = require('./routes/produtos-route');
const pedidoRoute = require('./routes/pedidos-route');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan("dev"));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

//Tell express-cassandra to use the models-directory, and
//use bind() to load the models using cassandra configurations.
models.setDirectory( __dirname + '/models').bind(
  {
      clientOptions: {
          contactPoints: ['127.0.0.1'],
          protocolOptions: { port: 9042 },
          keyspace: 'ecommerce',
          queryOptions: {consistency: models.consistencies.one}
      },
      ormOptions: {
          defaultReplicationStrategy : {
              class: 'SimpleStrategy',
              replication_factor: 1
          },
          migration: 'safe'
      }
  },
  function(err) {

      // You'll now have a `person` table in cassandra created against the model
      // schema you've defined earlier and you can now access the model instance
      // in `models.instance.Person` object containing supported orm operations.

      if(err) {console.log(err);}
  }
);

app.use('/uploads', express.static('uploads/'));

app.use('/usuarios', usuarioRoute);
app.use('/produtos', produtoRoute);
app.use('/pedidos', pedidoRoute);

// Caso seja acessada uma rota que não existe
app.use((req, res, next) => {
  const error = new Error("Route Not found");
  error.status = 404;
  next(error);
});

// Resposta padrão caso haja algum erro
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: error.message
  });
});

module.exports = app;