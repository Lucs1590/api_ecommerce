module.exports = {
    fields:{
        id      : "text",
        nome    : "text",
        endereco : "text",
        email : "text",
        senha     : "text",
        tipo      : "text",
        ativo     : "boolean",
        created : "timestamp"
    },
    key:["id"]
}