module.exports = {
    fields:{
        id          : "text",
        nome        : "text",
        preco       : "float",
        descricao   : "text",
        tag         : "text",
        imagem      : "text",
        created     : "timestamp"
    },
    key:["id"]
}