module.exports = {
    fields:{
        id_pedido         : "text",
        id_produto        : "text",
        id_usuario        : "text",
        nome_produto      : "text",
        preco_produto     : "float",
        descricao_produto : "text",
        tag_produto       : "text",
        imagem_produto    : "text",
        quantidade        : "int",
        finalizado        : "boolean",
        created           : "timestamp"
    },
    key:["id_pedido", 'id_usuario']
}