const express = require('express');
const router = express.Router();
const produtosController = require('../controllers/produtos-controller');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        var ext = file.originalname.substr(file.originalname.lastIndexOf('.') + 1);
        cb(null, file.fieldname + '-' + new Date().toISOString() + '.' + ext);
    }
});

const fileFilter = (req, file, cb) => {
    console.log(file.mimetype)
    if (['image/png', 'image/jpeg'].indexOf(file.mimetype) >= 0) {
        cb(null, true)
    } else {
        cb(new Error('Formato do arquivo inválido, permitido apenas jpg ou png'), false)
    }
};
const upload = multer({
    storage: storage,
    fileFilter: fileFilter
  });

router.get('/', produtosController.consultaProdutos);
router.get('/:id', produtosController.consultaUmProduto);
router.post('/', upload.single('file'), produtosController.insereProduto);
router.delete('/:id', produtosController.removerProduto);

module.exports = router;