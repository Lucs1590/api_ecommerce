const express = require('express');
const router = express.Router();
const userController = require('../controllers/usuarios-controller');

router.get('/', userController.consultaUsuarios);
router.get('/:id', userController.consultaUmUsuario);
router.post('/', userController.insereUsuario);
router.post('/login', userController.login);
router.delete('/:id', userController.removerUsuario);

module.exports = router;