const express = require('express');
const router = express.Router();
const pedidosController = require('../controllers/pedidos-controller');


router.get('/usuario/:id_usuario', pedidosController.consultaPedidos);
router.post('/', pedidosController.inserePedido);
router.delete('/:id_pedido', pedidosController.removerPedido);
router.post(
    '/finalizar/usuario/:id_usuario',
    pedidosController.finalizarPedidos,
    pedidosController.enviarEmailPedido);

module.exports = router;