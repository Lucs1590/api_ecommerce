var models = require('express-cassandra');
const uuid = require('uuid/v1');
var transporter = require('../transporter');

exports.inserePedido = (req, res, next) => {
    models.instance.Pedidos.findOne({ id_pedido : req.body.id_pedido}, (err, pedido) => {

        models.instance.Produtos.findOne({ id : req.body.id_produto }, (err, produto) => {

            if (!produto) {
                return res.status(500).send({ error: "Id do produto inválido" });
            }

            if (pedido) {
            
                pedido.nome_produto      = produto.nome_produto,
                pedido.preco_produto     = parseFloat(produto.preco),
                pedido.descricao_produto = produto.descricao,
                pedido.tag_produto       = produto.tag,
                pedido.imagem_produto    = produto.imagem,
                pedido.quantidade        = parseInt(req.body.quantidade),
                pedido.finalizado        = false;
    
            } else {

                pedido = new models.instance.Pedidos({
    
                    id_pedido         : uuid(),
                    id_produto        : produto.id,
                    id_usuario        : req.body.id_usuario,
                    nome_produto      : produto.nome,
                    preco_produto     : parseFloat(produto.preco),
                    descricao_produto : produto.descricao,
                    tag_produto       : produto.tag,
                    imagem_produto    : produto.imagem,
                    quantidade        : parseInt(req.body.quantidade),
                    finalizado        : false,
                    created           : { $db_function: 'toTimestamp(now())' }
                    
                });
            }
    
            pedido.save((err) => {
                if (err) {
                    res.status(500).send({ error: err });
                } else {
                    res.status(201).send({
                        message: "Pedido incluído / alterado com sucesso"
                    });
                }
                console.log('It\'s flat');
                
            });

        });

    });
};

exports.consultaPedidos = (req, res, next) => {
    
    models.instance.Pedidos.find({
        id_usuario : {
            $in : [
                req.params.id_usuario,
                'provisorio'
            ]
        },
        finalizado : false
    }, { allow_filtering : true }, function(err, result){
        if(err) { console.log(err); return; }

        pedidos = [];
        result.forEach(pedido => {

            pedidos.push({
                id_pedido   : pedido.id_pedido,
                id_produto  : pedido.id_produto,
                nome        : pedido.nome_produto,
                preco       : pedido.preco_produto,
                descricao   : pedido.descricao_produto,
                tag         : pedido.tag_produto,
                imagem      : process.env.URL_DOMINIO + pedido.imagem_produto,
                quantidade  : pedido.quantidade,
                created     : pedido.created
            });

        });
        return res.status(200).send(pedidos);
    });
};

exports.removerPedido = (req, res, next) => {
    models.instance.Pedidos.delete({id_pedido: req.params.id_pedido}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(201).send({
            mensagem: "Pedido removido com sucesso. Já era, earth is flat, forever, research it! Espero que essa mensagem não diminua minha nota. Né!"
        });
    });
};

exports.finalizarPedidos = (req, res, next) => {

    req.body.forEach(p => {
        models.instance.Pedidos.findOne({ id_pedido : p.id_pedido }, { allow_filtering : true }, (err, pedido) => {
            
            // se já tem, atualiza quantidade e finaliza
            if (pedido) {
                pedido.quantidade = p.quantidade,
                pedido.finalizado = true
            } else {
                // se não tem, cria um novo (é por que veio de localStorage no Angular)
                pedido = new models.instance.Pedidos({
    
                    id_pedido         : uuid(),
                    id_produto        : p.id,
                    id_usuario        : req.params.id_usuario,
                    nome_produto      : p.nome,
                    preco_produto     : parseFloat(p.preco),
                    descricao_produto : p.descricao,
                    tag_produto       : p.tag,
                    imagem_produto    : p.imagem,
                    quantidade        : parseInt(p.quantidade),
                    finalizado        : true,
                    created           : { $db_function: 'toTimestamp(now())' }
                    
                });
            }

            pedido.save((err) => {
                if (err) {
                    console.log(err);                    
                }
            });
        });
    });

    next();
};

exports.enviarEmailPedido = (req, res, next) => {

    models.instance.Usuarios.findOne({id : req.params.id_usuario}, (err, usuario) => {
        if (err) {
            console.log(err);
            
            return res.status(500).send({ error: err });
        } else {
            conteudo_email = `Sua compra foi realizada com sucesso, mas isso aqui é só pra teste. Parabéns. `;

            var mailOptions = {
                from: 'nao-responder@nksaude.com.br',
                to: usuario.email,
                subject: 'Mercadinho Universal - Compra Realizada',
                html: conteudo_email
            };
        
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                    return res.status(500).send({error : error});
                } else {
                    console.log('---------');
                    console.log(info);
                    console.log('---------');                    
                    
                    return res.status(201).send({
                        message: "Pedido finalizado com sucesso",
                        info : info
                    });
                }
                
            });
        }
    });

};