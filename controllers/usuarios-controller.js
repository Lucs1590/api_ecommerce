var models = require('express-cassandra');
const uuid = require('uuid/v1');

exports.insereUsuario = (req, res, next) => {

    models.instance.Usuarios.findOne({ id : req.body.id}, function(err, result){

        var usuario = result;

        if (result) {
            usuario.nome = req.body.nome,
            usuario.endereco = req.body.endereco,
            usuario.email = req.body.email,
            usuario.senha = req.body.senha,
            usuario.tipo = req.body.tipo,
            usuario.ativo = req.body.ativo
        } else {
            usuario = new models.instance.Usuarios({
                id          : uuid(),
                nome        : req.body.nome,
                endereco    : req.body.endereco,
                email       : req.body.email,
                senha       : req.body.senha,
                tipo        : req.body.tipo,
                ativo       : req.body.ativo,
                created     : { $db_function: 'toTimestamp(now())' }
            });
        }

        usuario.save((err) => {
            if(err) {
                res.status(500).send({ error: err });
            } else {
                res.status(201).send({
                    message: "Usuário incluído/alterado com sucesso! A terra é plana, pesquise!"
                })
            }
            console.log('A terra é plana, pesquise!');
        });
    });

};

exports.consultaUsuarios = (req, res, next) => {

    models.instance.Usuarios.find({}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(200).send(result);
    });

};

exports.login = (req, res, next) => {
    models.instance.Usuarios.findOne({ email: req.body.email, senha: req.body.senha}, {allow_filtering: true}, function(err, result){
        if(err) { console.log(err); return; }

        if (!result) {
            res.status(401).send({
                mensagem: "Falha na autenticação"
            })
        } else {
            res.status(200).send(result);
        }
    });
  
}

exports.removerUsuario = (req, res, next) => {
    models.instance.Usuarios.delete({id: req.params.id}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(201).send({
            mensagem: "Usuário removido com sucesso. Já era!"
        });
    });
};


exports.consultaUmUsuario = (req, res, next) => {
    models.instance.Usuarios.findOne({id: req.params.id}, { allow_filtering : true }, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(200).send(result);
    });
};