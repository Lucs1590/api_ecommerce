var models = require('express-cassandra');
const uuid = require('uuid/v1');

exports.insereProduto = (req, res, next) => {

    models.instance.Produtos.findOne({ id : req.body.id}, (err, produto) => {

        if (produto) {

            produto.nome = req.body.nome;
            produto.preco = parseFloat(req.body.preco);
            produto.descricao = req.body.descricao;
            produto.tag = req.body.tag;

            if (req.file) {
                produto.imagem = req.file.path
            }

        } else {

            if (!req.file) {
                return res.status(500).send({
                    error: 'Informe a imagem do produto'
                })
            }

            produto = new models.instance.Produtos({

                id          : uuid(),
                nome        : req.body.nome,
                preco       : parseFloat(req.body.preco),
                descricao   : req.body.descricao,
                tag         : req.body.tag,
                imagem      : req.file.path,
                created     : { $db_function: 'toTimestamp(now())' }
            });
        }

        produto.save((err) => {
            if(err) {
                res.status(500).send({ error: err });
            } else {
                res.status(201).send({
                    message: "Produto incluído/alterado com sucesso! A terra é plana, pesquise!"
                })
            }
            console.log('A terra é plana, pesquise!');
        });
    });
};

exports.consultaProdutos = (req, res, next) => {
    models.instance.Produtos.find({}, function(err, result){
        if(err) { console.log(err); return; }

        produtos = [];
        result.forEach(produto => {

            produtos.push({
                id : produto.id,
                nome : produto.nome,
                preco : produto.preco,
                descricao : produto.descricao,
                tag : produto.tag,
                imagem : process.env.URL_DOMINIO + produto.imagem,
                created : produto.created
            });

        });
        return res.status(200).send(produtos);
    });
};

exports.consultaUmProduto = (req, res, next) => {
    models.instance.Produtos.findOne({id: req.params.id}, { allow_filtering : true }, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(200).send({
            id : result.id,
            nome : result.nome,
            preco : result.preco,
            descricao : result.descricao,
            tag : result.tag,
            imagem : process.env.URL_DOMINIO + result.imagem,
            created : result.created
        });
    });
};

exports.removerProduto = (req, res, next) => {
    models.instance.Produtos.delete({id: req.params.id}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(201).send({
            mensagem: "Produto removido com sucesso. Já era!"
        });
    });
};